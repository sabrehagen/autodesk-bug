## The Bug

Assemblies of parts are translated correctly after the assembly is linked to its child parts using the `references/v1/setreference` api.

Assemblies of assemblies are not translated correctly after the assembly is linked to its child assemblies using the `references/v1/setreference` api.

Further, you'll notice code from line 39-45 deals with `500` errors from the Autodesk api. I recommend running the code a few times. I notice `500` errors approximately every 1 in 3 runs.

The bug relating to assemblies of assemblies could be my fault, and may not be a bug. I expect I am setting references of the child assemblies correctly as the same function works for setting references to child parts. If I am not setting references correctly using api, please let me know.

## Program Design

It has been suggested by Xiaodong that we should use the v2 api that allows you to upload a zip of files. We do not have a zip of files available to upload.

Our program is invoked with the name of the file that is to be translated. At invocation, we have no knowledge of other files required for translation. By translating an assembly with no supporting file references set, the manifest will report missing files. We use this information to search the local assembly directory for the missing files, upload them, set the references between the assembly and uploaded files, and re-translate the parent assembly.

This program is designed recursively to work with any level of assembly hierarchy.

## How to run

Add your `token` and `bucket` at line 15 and 16.

Run `npm install`.

Then run `node index.js`.

The code entrypoint is at line 295.

You can test any `urn` logged to the console using the [quick start guide](http://developer-autodesk.github.io/LmvQuickStart). All parts and assemblies translate successfully, except for the top level assembly of assemblies.

## Debugging

You can set `logHttpRequests` to `true` to log api calls to the Autodesk api to the console.