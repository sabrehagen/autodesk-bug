const fs = require('fs');
const path = require('path');
const walk = require('walk-sync');
const Promise = require('bluebird');
const requestLib = require('request');
const requestpLib = require('request-promise');
const chalk = require('chalk');

Promise.promisifyAll(fs);

const toBase64 = (str) => (new Buffer(str)).toString('base64');
const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
const inspect = (data) => require('util').inspect(data, { depth : 12 });

const token = 'm6MKAKxMZGHIl6vLlUQXyaTZOcmx';
const bucket = 'stemn';

const logHttpRequests = true;

// callback based http request handler
const request = (data, callback) => {

    // log calls to the api
    if (logHttpRequests) {
        console.log(chalk.green(data.method), chalk.cyan(data.url));
    }

    return requestLib(data, callback);
}

// promise based http request handler
const requestp = (data) => {

    // log calls to the api
    if (logHttpRequests) {
        console.log(chalk.green(data.method), chalk.cyan(data.url));
    }

    return requestpLib(data).catch((response) => {

        // sometimes the autodesk api returns 500 errors. if so, log them to the console for inspection.
        if (response.statusCode === 500) {
            console.log(chalk.red('\nAutodesk api returned 500\n'));
            console.log('## Request config:\n\n', data, '\n\n');
            console.log('## Autodesk response:\n\n', inspect(response.error), '\n\n');
            console.log('Retrying request...\n\n');

            return requestp(data);
        } else {
            return Promise.reject(response);
        }
    });
}

// used to construct a filename that conforms to autodesk's rules
const safeFilename = (filename) => `${Math.random().toString(36).substring(2)}${path.extname(filename)}`;

// gets the filename component of an urn
const urnFilename = (urn) => urn.slice(urn.lastIndexOf('/') + 1);

// checks if a given filename is an assembly
const isAssembly = (filename) => ['.sldasm', '.catproduct', '.iam'].includes(path.extname(filename).toLowerCase());

const uploadFile = (file) => {

    const sourceFile = fs.createReadStream(file);

    return new Promise((resolve, reject) => {

        const filename = path.basename(file);
        const uploadFilename = safeFilename(filename);

        const object = request({
            method : 'PUT',
            url : `https://developer.api.autodesk.com/oss/v2/buckets/${bucket}/objects/${uploadFilename}`,
            headers : {
                Authorization : `Bearer ${token}`,
                'Content-Type' : 'application/octet-stream'
            },
            json : true
        }, (err, response, body) => {

            if (err) {
                reject(err);
            } else {

                console.log(`Uploaded ${chalk.yellow(filename)} to ${chalk.green(body.objectId)}`);

                resolve({
                    urn : body.objectId,
                    filename,
                    file
                });
            }
        });

        // pipe the file to the autodesk object
        sourceFile.pipe(object);
    });
}

const getManifest = (data) => {

    // recursively traverse all children of the manifest and check they are either completed or failed
    const isConversionFinished = (info) => {

        // this is a workaround to wait until the manifest api returns the full manifest, not a partial manifest
        // see https://github.com/cyrillef/extract.autodesk.io/issues/12
        const isTopLevel = info.owner;
        if (isTopLevel) {
            const viewableData = info.children.find((child) => child.role === 'viewable');
            if (viewableData && viewableData.name && viewableData.children) {
                const nonSceneFormats = ['.dwfx', '.dwg', '.idw'];
                const sceneFormat = path.extname(viewableData.name);
                const isNonSceneFormat = nonSceneFormats.includes(sceneFormat);
                if (isNonSceneFormat && !viewableData.children) {
                    return true; // this file is never going to have scene data
                }

                const sceneData = viewableData.children.find((child) => child.name === 'Scenes');
                if (!sceneData) {
                    return false; // no scene data added to manifest yet, wait until it is
                }
            } else {
                return false;
            }
        }
        // end autodesk workaround

        // only check the status if the status property exists (not all children require conversion checks)
        const finishedConversion = info.status
            ? ['success', 'failed'].includes(info.status)
            : true;

        // only check the status of the children's conversion if they exist
        const childrenFinishedConversion = info.children
            ? info.children.map(isConversionFinished).every((complete) => complete)
            : true;

        return finishedConversion && childrenFinishedConversion;
    }

    return requestp({
        method : 'GET',
        headers : {
            Authorization : `Bearer ${token}`
        },
        url : `https://developer.api.autodesk.com/derivativeservice/v2/manifest/${toBase64(data.urn)}`,
        json : true
    }).then((info) => isConversionFinished(info)
        ? info
        : wait(500).then(() => getManifest(data))
    );
}

const register = (data) => {

    const registerFile = (data) => {

        return requestp({
            method : 'POST',
            url : 'https://developer.api.autodesk.com/viewingservice/v1/register',
            headers : {
                Authorization : `Bearer ${token}`,
                'x-ads-force' : data.force // forces re-translation of a previously translated file
            },
            body : {
                urn : toBase64(data.urn)
            },
            json : true
        }).then((response) => ['Success', 'Created'].includes(response.Result)
            ? getManifest(data).then(() => data)
            : Promise.reject(new Error('Render failed!'))
        );
    }

    const registerAssembly = (data) => {

        // register the assembly so we can get it's urn to give to the find assembly parts function
        return registerFile(data).then((assembly) => {

            // get the part files used in the assembly
            return getAssemblyParts(assembly).then((assemblyParts) => {

                console.log(`Assembly ${chalk.magenta(data.filename)} is missing supporting files ${chalk.cyan(assemblyParts.join(', '))}`);

                return Promise.map(assemblyParts, (part) => {

                    // find the part in the assembly folder
                    return findPartFile({ part, assembly }).then((partFile) => {

                        // translate part file
                        return translate(partFile);
                    });
                })
                .then((assemblyParts) => {

                    // set references for assembly 1 to the part files
                    return setReferences({
                        urn : assembly.urn,
                        filename : assembly.filename,
                        parts : assemblyParts
                    })
                    .then(() => {

                        // we must force re-translation of the assembly as the first translation failed (when we got the part files from the manifest)
                        const assemblyWithForce = Object.assign({ force : true }, assembly);

                        return registerFile(assemblyWithForce);
                    });
                });
            });
        });
    }

    return isAssembly(data.filename)
        ? registerAssembly(data)
        : registerFile(data);
}

const setReferences = (data) => {

    console.log(`Setting references for ${chalk.magenta(data.filename)} to ${chalk.cyan(data.parts.map(part => part.filename).join(', '))}`);

    return requestp({
        method : 'POST',
        headers : {
            Authorization : `Bearer ${token}`
        },
        url : 'https://developer.api.autodesk.com/references/v1/setreference',
        body : {
            master : data.urn,
            dependencies : data.parts.map((part) => ({
                file : part.urn,
                metadata : {
                    childPath : part.filename,
                    parentPath : urnFilename(data.urn)
                }
            }))
        },
        json : true
    });

}

const getAssemblyParts = (data) => getManifest(data).then((manifest) => {

    // filenames referenced in assemblies have the following error message
    const partFiles = (error) => error.message[0] === 'The file: {0} does not exist.';

    // assemblies are windows only, so use win32 path library to extract filename
    const pickFilename = (error) => path.win32.basename(error.message[1]);

    // get the part files referenced by the assembly
    const supportingFiles = manifest.children[0].children.find((child) => child.type === 'folder').children[0].messages
    .filter(partFiles)
    .map(pickFilename);

    return supportingFiles;
});

const findPartFile = (data) => {

    const assemblyPath = path.dirname(data.assembly.file);

    // list all files in the assembly folder
    const filenames = walk(assemblyPath);

    // pick the file that matches the given part name
    const partFilename = filenames.find((filename) => path.basename(filename) === data.part);

    return partFilename
        ? Promise.resolve(path.join(assemblyPath, partFilename))
        : Promise.reject('Assembly part not found locally!');
}

const translate = (data) => {

    // upload the file to be rendered
    return uploadFile(data).then((file) => {

        // register the file for translation
        return register(file).then((translation) => {

            console.log(`Translated ${chalk.yellow(translation.filename)}. urn: ${chalk.green(translation.urn)}`);

            return translation;
        });
    });
}

const assembly = './Assembly/Assembly of Assemblies.iam';

// translate the given assembly
return translate(assembly).then((translation) => {

    // get the manifest for the failed assembly translation
    return getManifest(translation).then((manifest) => {

        console.log();
        console.log(inspect(manifest));
        console.log(chalk.red('\n\nFailed translation manifest above shows missing referenced parts despite correctly setting references\n'));

    });
});
